<?php
namespace VicentGodella\OpenGestia\OpenGestiaBundle\Tests\Entity;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use VicentGodella\OpenGestia\OpenGestiaBundle\Entity\ConfiguracionUsuario;

class ConfiguracionUsuarioTest extends WebTestCase
{
    protected $configuracionUsuario;
    protected static $validator;
    protected static $kernel;

    public static function setUpBeforeClass()
    {
        $kernel = self::getKernelClass();
        $kernel = new $kernel('dev', true);
        $kernel->boot();

        self::$kernel = $kernel;
        self::$validator = $kernel->getContainer()->get('validator');
    }

    protected function setUp()
    {
        $this->configuracionUsuario = new ConfiguracionUsuario();
    }

    public function testConfiguracionNinyosPorDefecto()
    {
	    $defaultConfig = $this->configuracionUsuario->getCampos('ninyos');

        $this->assertEquals(6, count($defaultConfig));
    }

	public function testConfiguracionAnyadirCampoNinyosPorDefecto()
	{
		$config = $this->configuracionUsuario;

		$config->anyadirCampo('ninyos', 'dni');


		$this->assertEquals(7, count($config->getCampos('ninyos')));
	}

	public function testConfiguracionEliminarCampoNinyosPorDefecto()
	{
		$config = $this->configuracionUsuario;

		$config->eliminarCampo('ninyos', 'fechaNacimiento');

		$this->assertEquals(5, count($config->getCampos('ninyos')));
	}

	public function testConfiguracionEducadoresPorDefecto()
	{
		$defaultConfig = $this->configuracionUsuario->getCampos('educadores');

		$this->assertEquals(6, count($defaultConfig));
	}
	
	public function testConfiguracionAnyadirCampoEducadoresPorDefecto()
	{
		$config = $this->configuracionUsuario;

		$config->anyadirCampo('educadores', 'dni');


		$this->assertEquals(7, count($config->getCampos('educadores')));
	}

	public function testConfiguracionEliminarCampoEducadoresPorDefecto()
	{
		$config = $this->configuracionUsuario;

		$config->eliminarCampo('educadores', 'fechaNacimiento');

		$this->assertEquals(5, count($config->getCampos('educadores')));
	}
}
