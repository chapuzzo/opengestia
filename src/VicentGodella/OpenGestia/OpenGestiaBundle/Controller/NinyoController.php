<?php

namespace VicentGodella\OpenGestia\OpenGestiaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use VicentGodella\OpenGestia\OpenGestiaBundle\Entity\Ninyo;
use VicentGodella\OpenGestia\OpenGestiaBundle\Form\Type\NinyoFormType;
use VicentGodella\OpenGestia\OpenGestiaBundle\Form\Type\NinyoEditFormType;
use VicentGodella\OpenGestia\OpenGestiaBundle\Entity\ConfiguracionUsuario;

class NinyoController extends Controller
{
    public function indexAction()
    {
        $em = $this->get('doctrine')->getEntityManager();
        $ninyos = $em->getRepository('OpenGestiaBundle:Ninyo')->findActivos();

	    $campos_disponibles = ConfiguracionUsuario::getCamposDisponibles();

        return $this->render('OpenGestiaBundle:Ninyo:index.html.twig', array(
                'ninyos' => $ninyos,
		        'campos_disponibles' => $campos_disponibles['ninyos']
            )
        );
    }

	public function bajasDefinitivasAction()
	{
		$em = $this->get('doctrine')->getEntityManager();
		$ninyos = $em->getRepository('OpenGestiaBundle:Ninyo')->findBajasDefinitivas();

		$campos_disponibles = ConfiguracionUsuario::getCamposDisponibles();

		return $this->render('OpenGestiaBundle:Ninyo:index.html.twig', array(
				'ninyos' => $ninyos,
				'campos_disponibles' => $campos_disponibles['ninyos']
			)
		);
	}

	public function bajasAction()
	{
		$em = $this->get('doctrine')->getEntityManager();
		$ninyos = $em->getRepository('OpenGestiaBundle:Ninyo')->findBajas();

		$campos_disponibles = ConfiguracionUsuario::getCamposDisponibles();

		return $this->render('OpenGestiaBundle:Ninyo:index.html.twig', array(
				'ninyos' => $ninyos,
				'campos_disponibles' => $campos_disponibles['ninyos']
			)
		);
	}

    public function bajasSimultaneasAction()
    {
        $em = $this->get('doctrine')->getEntityManager();
        $personaManager = $this->get('opengestia.persona_manager');

        $ninyosIds = $this->getRequest()->request->get('ids');

        $ninyos = $em->getRepository('OpenGestiaBundle:Ninyo')->findNinyosByIds($ninyosIds);

        foreach ($ninyos as $ninyo) {
            $personaManager->darDeBaja($ninyo);
        }

        return new Response('');
    }

    public function bajasDefinitivasSimultaneasAction()
    {
        $em = $this->get('doctrine')->getEntityManager();
        $personaManager = $this->get('opengestia.persona_manager');

        $ninyosIds = $this->getRequest()->request->get('ids');

        $ninyos = $em->getRepository('OpenGestiaBundle:Ninyo')->findNinyosByIds($ninyosIds);

        foreach ($ninyos as $ninyo) {
            $personaManager->darDeBajaDefinitiva($ninyo);
        }

        return new Response('');
    }

    public function newAction()
    {
        $request = $this->get('request');
        $em = $this->get('doctrine')->getEntityManager();
        $personaManager = $this->get('open_gestia.persona_manager');
        $duplicidad = false;

        $user = $this->get('security.context')->getToken()->getUser();

        $persona = new Ninyo();
        $persona->setCentro($user->getCentro());
        $formulario = $this->createForm(new NinyoFormType(), $persona);

        if ($request->getMethod() == 'POST') {
            $formulario->bind($request);

            if ($formulario->isValid()) {
                if (!$request->get('duplicidad') && $personaManager->existePersonaEnCentro($persona)) {
                    $this->get('session')->setFlash('warning', 'Es posible que este niño ya esté en el centro, debes confirmar el guardado');

                    $duplicidad = true;
                } else {
                    $this->get('session')->setFlash('notice', 'Se ha creado el niño satisfactoriamente');

                    $em->persist($persona);
                    $em->flush();
                    
                    return $this->redirect($this->generateUrl('ninyo_index'));   
                }
            }
        }

	    $template = 'OpenGestiaBundle:Ninyo:new.html.twig';

	    if ($request->isXmlHttpRequest()) {
		    $template = 'OpenGestiaBundle:Ninyo:new_form.html.twig';
	    }

        return $this->render($template,
            array(
                'formulario' => $formulario->createView(),
                'duplicidad' => $duplicidad
            )
        );
    }

	public function editAction($id)
	{
		$request = $this->get('request');
		$em = $this->get('doctrine')->getEntityManager();

		$persona = $this->getDoctrine()->getManager()->getRepository('OpenGestiaBundle:Ninyo')->find($id);

		$formulario = $this->createForm(new NinyoEditFormType(), $persona);

		if ($request->getMethod() == 'POST') {
			$formulario->bind($request);

			if ($formulario->isValid()) {
				$this->get('session')->setFlash('notice', 'Se han guardado los datos del niño satisfactoriamente');

				$em->persist($persona);
				$em->flush();

				return $this->redirect($this->generateUrl('ninyo_index'));
			}
		}

		$template = 'OpenGestiaBundle:Ninyo:edit.html.twig';

		if ($request->isXmlHttpRequest()) {
			$template = 'OpenGestiaBundle:Ninyo:edit_form.html.twig';
		}

		return $this->render($template,
			array(
				'formulario' => $formulario->createView()
			)
		);
	}

	public function ascenderNinyoEducadorAction($id)
	{
		$em = $this->get('doctrine')->getEntityManager();

		$ninyo = $this->getDoctrine()->getManager()->getRepository('OpenGestiaBundle:Ninyo')->find($id);

		if ($ninyo === null) {
			return new Response('No existe ningún niño con este identificador', 404);
		}

		$em->getConnection()->update('Persona', array('type' => 'educador'), array('id' => $id));

		return $this->redirect($this->generateUrl('ninyo_index'));

	}
	public function anyadirCampoAction($campo)
	{
		$em = $this->get('doctrine')->getEntityManager();

		$usuario = $this->container->get('security.context')->getToken()->getUser();

		$configuracionUsuario = $usuario->getConfiguracion();
		$configuracionUsuario->anyadirCampo('ninyos', $campo);

		$em->persist($configuracionUsuario);
		$em->flush($configuracionUsuario);

		return $this->indexAction();
	}

	public function eliminarCampoAction($campo)
	{
		$em = $this->get('doctrine')->getEntityManager();

		$usuario = $this->container->get('security.context')->getToken()->getUser();

		$configuracionUsuario = $usuario->getConfiguracion();
		$configuracionUsuario->eliminarCampo('ninyos', $campo);

		$em->persist($configuracionUsuario);
		$em->flush($configuracionUsuario);

		return $this->indexAction();
	}
}
