<?php
namespace VicentGodella\OpenGestia\OpenGestiaBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class DNIConstraint extends Constraint
{
    public $message = 'Dni incorrecto';
}
