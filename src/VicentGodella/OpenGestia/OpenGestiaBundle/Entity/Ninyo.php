<?php

namespace VicentGodella\OpenGestia\OpenGestiaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use VicentGodella\OpenGestia\OpenGestiaBundle\Validator\Constraints as OpenGestiaAssert;

/**
 * VicentGodella\OpenGestiaBundle\Entity\Ninyo
 *
 * @ORM\Entity(repositoryClass="VicentGodella\OpenGestia\OpenGestiaBundle\Entity\NinyoRepository")
 */
class Ninyo extends Persona implements UserInterface, \Serializable
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

	/**
	 * @ORM\Column(name="cuotaPagada", type="boolean")
	 */
	protected $cuotaPagada = false;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

	public function getTipoPersona()
	{
		return 'niño';
	}

	public function setCuotaPagada($cuotaPagada)
	{
		$this->cuotaPagada = $cuotaPagada;
	}

	public function getCuotaPagada()
	{
		return $this->cuotaPagada;
	}

}