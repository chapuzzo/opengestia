<?php

namespace VicentGodella\OpenGestia\OpenGestiaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use VicentGodella\OpenGestia\OpenGestiaBundle\Validator\Constraints as OpenGestiaAssert;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * VicentGodella\OpenGestiaBundle\Entity\Grupo
 *
 * @ORM\Table(name="Grupo")
 * @ORM\Entity
 * @UniqueEntity(fields={"nombre","centro"}, message="El nombre del grupo no se puede repetir en un centro")
 */
class Grupo
{
	const GRUPO_PACTO = 'Pacto';
	const GRUPO_IDENTIDAD = 'Identidad';
	const GRUPO_EXPERIENCIA = 'Experiencia';
	const GRUPO_ESTILO_DE_VIDA = 'Estilo de vida';

	public static function getTiposDeGrupo()
	{
		return array(
			self::GRUPO_PACTO => self::GRUPO_PACTO,
			self::GRUPO_IDENTIDAD => self::GRUPO_IDENTIDAD,
			self::GRUPO_EXPERIENCIA => self::GRUPO_EXPERIENCIA,
			self::GRUPO_ESTILO_DE_VIDA => self::GRUPO_ESTILO_DE_VIDA
		);
	}

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="nombre", type="string")
     * @Assert\NotBlank()
     */
    protected $nombre;

	/**
	 * @ORM\Column(name="tipo", type="string")
	 * @Assert\NotBlank()
	 */
	protected $tipo;

	/**
	 * @ORM\OneToMany(targetEntity="Educador", mappedBy="grupo")
	 */
	protected $educadores;

	/**
	 * @ORM\Column(name="centro", type="string")
	 * @Assert\NotBlank()
	 */
	protected $centro;

	public function __toString()
	{
		return $this->nombre;
	}

    public function __construct()
    {
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param  integer $nombre
     * @return Grupo
     */
    public function setNombre($nombre)
    {
        $this->nombre = ucfirst($nombre);

        return $this;
    }

    /**
     * Get nombre
     *
     * @return integer
     */
    public function getNombre()
    {
        return $this->nombre;
    }

	public function setTipo($tipo)
	{
		$this->tipo = $tipo;
	}

	public function getTipo()
	{
		return $this->tipo;
	}

	public function setCentro($centro)
	{
		$this->centro = $centro;
	}

	public function getCentro()
	{
		return $this->centro;
	}

}