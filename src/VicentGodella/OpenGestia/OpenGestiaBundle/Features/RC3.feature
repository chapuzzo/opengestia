# language: es
Característica: RC3-Saber-Niño-Cuota-Pagada
	Como responsable de centro
	Quiero saber si un niño ha pagado la cuota del curso
	Para pedírsela si no lo ha hecho

 	Antecedentes:
		Dados prepara antecedentes

	@orm @javascript
	Escenario: un niño ha pagado la cuota
		 Dado Un usuario autenticado como "responsable de centro"
			Y el usuario tiene la configuración por defecto
	   Cuando Estoy en la página de listado de niños de centro
			Y añado el campo "cuotaPagada" desde el panel
	 Entonces Veo los campos del listado de "ninyos" definidos por defecto más los siguientes campos:
			"""""""""""""
			cuotaPagada
			"""""""""""""
