# language: es
Característica: RC5-Actualizar-Contacto-Educador
	Como responsable de centro
	Quiero mantener actualizados los datos de contacto de un educador
	Para ponerme en contacto con él cuando cambie alguno de dichos datos

 	Antecedentes:
		Dados prepara antecedentes

	@orm
	Escenario: actualizar datos contacto de educador correctamente
		Dado Un usuario autenticado como "responsable de centro"
		  Y Estoy en la página de listado de educadores de centro
	     Cuando Seleccione la ficha del primer educador
		  Y actualice los datos del educador correctamente
		  Y Haga click en guardar
		 Entonces Debería ver los datos del educador actualizados en el listado de educadores del centro

    @orm
	Escenario: muestra error si el teléfono actualizado del educador es incorrecto
		Dado Un usuario autenticado como "responsable de centro"
	      Y Estoy en la página de listado de educadores de centro
	     Cuando Seleccione la ficha del primer educador
		  Y relleno incorrectamente el número de teléfono al actualizar el educador
		  Y Haga click en guardar
	    Entonces Debería ver un elemento de error para "telefono"

	@orm
	Escenario: muestra error si el dni actualizado del educador es incorrecto
		Dado Un usuario autenticado como "responsable de centro"
		  Y Estoy en la página de listado de educadores de centro
		 Cuando Seleccione la ficha del primer educador
		  Y relleno incorrectamente el dni al actualizar el educador
		  Y Haga click en guardar
		 Entonces Debería ver un elemento de error para "dni"

	@orm
	Escenario: muestra error si el correo actualizado electrónico del educador es incorrecto
		Dado Un usuario autenticado como "responsable de centro"
		  Y Estoy en la página de listado de educadores de centro
		 Cuando Seleccione la ficha del primer educador
		  Y relleno incorrectamente el correo electrónico al actualizar el educador
		  Y Haga click en guardar
		 Entonces Debería ver un elemento de error para "email"

	@orm
	Escenario: muestra error si la fecha de nacimiento actualizado electrónico del educador es incorrecto
		Dado Un usuario autenticado como "responsable de centro"
		  Y Estoy en la página de listado de educadores de centro
		 Cuando Seleccione la ficha del primer educador
		  Y relleno incorrectamente la fecha de nacimiento al actualizar el educador
		  Y Haga click en guardar
		 Entonces Debería ver un elemento de error para "fecha_nacimiento"
