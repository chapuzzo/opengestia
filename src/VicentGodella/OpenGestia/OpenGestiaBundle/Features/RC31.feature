# language: es
Característica: RC31 – Añadir-Campos-A-Listado-Educadores
	Como responsable de centro
	Quiero añadir campos al listado de educadores
	Para obtener más información de un vistazo

	Antecedentes:
		Dados prepara antecedentes

	@orm
	Escenario: mostrar campos por defecto
		 Dado Un usuario autenticado como "responsable de centro"
		    Y el usuario tiene la configuración por defecto
	   Cuando Estoy en la página de listado de niños de centro
	 Entonces Veo los campos del listado de "educadores" definidos por defecto

	@orm
	Escenario: añadimos un campo y aparece en el listado de educadores
		 Dado Un usuario autenticado como "responsable de centro"
			Y el usuario tiene la configuración por defecto
	   Cuando Estoy en la página de listado de educadores de centro
			Y añado el campo "codigoPostal" para "educador"
	 Entonces Veo los campos del listado de "educadores" definidos por defecto más los siguientes campos:
			"""""""""""""
			codigoPostal
			"""""""""""""
#
#	@orm
#	Escenario: añadimos un campo desde el desplegable y aparece en el listado de educadores
#		 Dado Un usuario autenticado como "responsable de centro"
#			Y el usuario tiene la configuración por defecto
#	   Cuando Estoy en la página de listado de educadores de centro
#			Y añado el campo "email" desde el panel
#	 Entonces Veo los campos del listado de "educadores" definidos por defecto más los siguientes campos:
#			"""""""""""""
#			email
#			"""""""""""""
