# language: es
Característica: RC36-Baja-Educadores
	  Como responsable de centro
	Quiero dar de baja varios educadores simultáneamente
	  Para ejecutar la baja de varios educadores cómodamente

 	Antecedentes:
		Dados prepara antecedentes

    @orm @javascript
	Escenario: marca educadores como baja correctamente
		Dados Un usuario autenticado como "responsable de centro"
	  	    Y Estoy en la página de listado de educadores de centro
	   Cuando selecciono una persona del listado
		    Y hago click en baja desde el panel
	 Entonces No debería ver los datos del educador en el listado de educadores del centro
