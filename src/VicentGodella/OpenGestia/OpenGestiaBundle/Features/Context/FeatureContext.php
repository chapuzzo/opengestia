<?php
namespace VicentGodella\OpenGestia\OpenGestiaBundle\Features\Context;

use Symfony\Component\HttpKernel\KernelInterface;
use Behat\Symfony2Extension\Context\KernelAwareInterface;
use PSS\Behat\Symfony2MockerExtension\Context\ServiceMockerAwareInterface;
use PSS\Behat\Symfony2MockerExtension\ServiceMocker;
use Behat\MinkExtension\Context\RawMinkContext;
use Behat\MinkExtension\Context\MinkContext;
use Behat\MinkExtension\Context\MinkDictionary;

use Behat\Behat\Event\ScenarioEvent;

use Behat\Behat\Context\BehatContext,
    Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode,
    Behat\Gherkin\Node\TableNode;
use Behat\Behat\Context\Step\Given,
    Behat\Behat\Context\Step\When,
    Behat\Behat\Context\Step\Then;

use Behat\Mink\WebAssert;

use VicentGodella\OpenGestia\OpenGestiaBundle\Entity\Persona;
use VicentGodella\OpenGestia\OpenGestiaBundle\Entity\Educador;
use VicentGodella\OpenGestia\OpenGestiaBundle\Entity\Ninyo;
use VicentGodella\OpenGestia\OpenGestiaBundle\Entity\ConfiguracionUsuario;
use VicentGodella\OpenGestia\OpenGestiaBundle\Entity\Grupo;

/**
 * Feature context.
 */
class FeatureContext extends MinkContext
                  implements KernelAwareInterface, ServiceMockerAwareInterface
{
    private $kernel;
    private $parameters;

	/**
	 * @var \PSS\Behat\Symfony2MockerExtension\ServiceMocker $mocker
	 */
	private $mocker = null;

	/**
	 * @param \PSS\Behat\Symfony2MockerExtension\ServiceMocker $mocker
	 *
	 * @return null
	 */
	public function setServiceMocker(ServiceMocker $mocker)
	{
		$this->mocker = $mocker;
	}

    /**
     * Initializes context with parameters from behat.yml.
     *
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * Sets HttpKernel instance.
     * This method will be automatically called by Symfony2Extension ContextInitializer.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @BeforeScenario @database, @orm
     */
    public function cleanDB(ScenarioEvent $event)
    {
		$em = $this->kernel->getContainer()->get('doctrine')->getEntityManager();

		$users = $em->getRepository('OpenGestiaBundle:Persona')->findAll();
        foreach ($users as $user) {
            $em->remove($user);
        }

	    $grupos = $em->getRepository('OpenGestiaBundle:Grupo')->findAll();
	    foreach ($grupos as $grupo) {
		    $em->remove($grupo);
	    }

        $em->flush();
    }

	public function gruposEnElSistema()
	{
		$em = $this->kernel->getContainer()->get('doctrine')->getEntityManager();

		$grupo = new Grupo();
		$grupo->setTipo(Grupo::GRUPO_PACTO);
		$grupo->setCentro('mi centro');
		$grupo->setNombre('Las marmotas');

		$em->persist($grupo);
		$em->flush();

		$grupo = new Grupo();
		$grupo->setTipo(Grupo::GRUPO_IDENTIDAD);
		$grupo->setCentro('mi centro');
		$grupo->setNombre('Los lobos solitarios');

		$em->persist($grupo);
		$em->flush();
	}

	/**
	 * @Given /^prepara antecedentes de educadores$/
	 */
	public function preparaAntecedentesDeEducadores()
	{
		$em = $this->kernel->getContainer()->get('doctrine')->getEntityManager();

		$this->gruposEnElSistema();

		$educadores = array(
			array(
				'nombre' => 'pepe',
				'apellidos' => 'martínez',
				'fecha_nacimiento' => '1980/02/06',
				'direccion' => 'C/ Imposible, 3',
				'cp' => '46001',
				'poblacion' => 'Valencia',
				'password' => 'secret',
				'email' => 'pepe@opengestia.org',
				'telefono' => '91222333',
				'dni' => '20000000M',
				'centro' => 'mi centro',
				'grupo' => $em->getRepository('OpenGestiaBundle:Grupo')->findOneBy(
					array(
						'nombre' => 'Las marmotas',
						'centro' => 'mi centro'
					)
				)
			),
			array(
				'nombre' => 'juan',
				'apellidos' => 'rodríguez',
				'fecha_nacimiento' => '1985/09/16',
				'direccion' => 'C/ Del medio, 16',
				'cp' => '46100',
				'poblacion' => 'Burjassot',
				'password' => 'password',
				'email' => 'juan@opengestia.org',
				'telefono' => '91333444',
				'dni' => '32000000P',
				'centro' => 'mi centro',
				'grupo' => $em->getRepository('OpenGestiaBundle:Grupo')->findOneBy(
					array(
						'nombre' => 'Los lobos solitarios',
						'centro' => 'mi centro'
					)
				),
			)
		);

		$this->educadoresEnElSistema($educadores);
	}

	/**
	 * @Given /^prepara antecedentes$/
	 */
	public function preparaAntecedentes()
	{
		$this->preparaAntecedentesDeEducadores();

		$ninyos = array(
			array(
				'nombre' => 'Antonio',
				'apellidos' => 'García Márquez',
				'fecha_nacimiento' => '1998/05/06',
				'direccion' => 'C/ Imposible, 3',
				'cp' => '46001',
				'poblacion' => 'Valencia',
				'password' => 'secret',
				'email' => 'agarcia@opengestia.org',
				'telefono' => '96111222',
				'dni' => '20000000M',
				'centro' => 'mi centro',
				'grupo' => 'Las marmotas'
			),
			array(
				'nombre' => 'Federico',
				'apellidos' => 'Pérez Iniesta',
				'fecha_nacimiento' => '1999/09/16',
				'direccion' => 'C/ Del medio, 16',
				'cp' => '46100',
				'poblacion' => 'Burjassot',
				'password' => 'password',
				'email' => 'fperez@opengestia.org',
				'telefono' => '96222333',
				'dni' => '32000000P',
				'centro' => 'mi centro',
				'grupo' => 'Los lobos solitarios'
			),
			array(
				'nombre' => 'Santiago',
				'apellidos' => 'Moreno Perales',
				'fecha_nacimiento' => '1999/08/06',
				'direccion' => 'C/ Del medio, 46',
				'cp' => '46100',
				'poblacion' => 'Burjassot',
				'password' => 'password',
				'email' => 'smoreno@opengestia.org',
				'telefono' => '96888777',
				'dni' => '32000000P',
				'centro' => 'mi centro',
				'grupo' => 'Las marmotas'
			),
		);

		$this->ninyosEnElSistema($ninyos);
	}

    public function educadoresEnElSistema($table)
    {
        $em = $this->kernel->getContainer()->get('doctrine')->getEntityManager();
        $factory = $this->kernel->getContainer()->get('security.encoder_factory');

        foreach ($table as $hash) {
            $user = new Educador();
            $encoder = $factory->getEncoder($user);
            $password = $encoder->encodePassword($hash['password'], $user->getSalt());
            $user->setPassword($password);
            $user->setNombre($hash['nombre']);
            $user->setEmail($hash['email']);
            $user->setApellidos($hash['apellidos']);
            $user->setFechaNacimiento(new \Datetime($hash['fecha_nacimiento']));
            $user->setDireccion($hash['direccion']);
            $user->setCodigoPostal($hash['cp']);
            $user->setPoblacion($hash['poblacion']);
            $user->setCentro($hash['centro']);
			$user->setTelefono($hash['telefono']);
			$user->setGrupo($hash['grupo']);
          //  $user->addRole('ROLE_RESPONSABLE_CENTRO');

            $em->persist($user);
        }

        $em->flush();
    }

    public function ninyosEnElSistema($table)
    {
        $em = $this->kernel->getContainer()->get('doctrine')->getEntityManager();
        $factory = $this->kernel->getContainer()->get('security.encoder_factory');

        foreach ($table as $hash) {
            $user = new Ninyo();
            $encoder = $factory->getEncoder($user);
            $password = $encoder->encodePassword($hash['password'], $user->getSalt());
            $user->setPassword($password);
            $user->setNombre($hash['nombre']);
            $user->setEmail($hash['email']);
            $user->setApellidos($hash['apellidos']);
            $user->setFechaNacimiento(new \Datetime($hash['fecha_nacimiento']));
            $user->setDireccion($hash['direccion']);
            $user->setCodigoPostal($hash['cp']);
            $user->setPoblacion($hash['poblacion']);
            $user->setCentro($hash['centro']);
			$user->setTelefono($hash['telefono']);
			$user->setGrupo($em->getRepository('OpenGestiaBundle:Grupo')->findOneBy(
				array(
					'nombre' => $hash['grupo'],
					'centro' => $hash['centro']
				)
			));

			$em->persist($user);
        }

        $em->flush();
    }

    /**
     * @Given /^Estoy en "([^"]*)"$/
     */
    public function estoyEn($url)
    {
        return new Given("I am on \"$url\"");
    }

    /**
     * @When /^Relleno "([^"]*)" con "([^"]*)"$/
     */
    public function rellenoCon($field, $value)
    {
        return new Given("relleno \"$field\" con \"$value\"");
    }

    /**
     * @When /^Hago click en "([^"]*)"$/
     */
    public function clickEn($field)
    {
        return new Given("I press \"$field\"");
    }

	/**
	 * @Given /^"([^"]*)" está autenticado como "([^"]*)"$/
	 */
	public function autenticado($email, $rol)
	{
		if($email !== $this->kernel->getContainer()->get('security.context')->getToken()->getUsername())
			throw new \Exception("no autenticado");
	}

	/**
     * @Given /^Un usuario autenticado como "([^"]*)"$/
     */
    public function unUsuarioAutenticadoComo($rol)
    {
        return array(
            new Given("Estoy en \"/login\""),
            new When("relleno \"username\" con \"pepe@opengestia.org\""),
            new When("relleno \"password\" con \"secret\""),
            new When("presiono \"boton_login\"")
        );
    }

    /**
     * @Given /^Estoy en la página de nuevos datos de contacto de niño$/
     */
    public function estoyEnLaPaginaDeNuevosDatosDeContactoDeNino()
    {
        return new When("Estoy en \"/ninyo/new\"");
    }

    /**
     * @When /^rellene los datos del niño correctamente$/
     */
    public function relleneLosDatosDelNinoCorrectamente()
    {
        return array(
            new When("Relleno \"ninyo_nombre\" con \"Enrique\""),
            new When("Relleno \"ninyo_apellidos\" con \"Martínez Navarro\""),
            new When("Relleno \"ninyo_fecha_nacimiento\" con \"02/10/2000\""),
            new When("Relleno \"ninyo_direccion\" con \"C/ Perdida, 4\""),
            new When("Relleno \"ninyo_codigo_postal\" con \"46000\""),
            new When("Relleno \"ninyo_telefono\" con \"96123456\""),
            new When("Relleno \"ninyo_poblacion\" con \"Valencia\""),
            new When("selecciono \"Las marmotas\" de \"ninyo_grupo\""),
        );
    }

    /**
     * @When /^Haga click en guardar$/
     */
    public function hagaClickEnGuardar()
    {
        return new When("I press \"Guardar\"");
    }

    /**
     * @Then /^Debería ver los datos del niño en el listado de niños del centro$/
     */
    public function deberiaVerLosDatosDelNinoEnElListadoDeNinosDelCentro()
    {
        return array(
            new Then("I should be on \"/ninyo/\""),
            new Then("I should see \"Enrique\""),
            new Then("I should see \"Martínez Navarro\"")
        );
    }

    /**
     * @When /^relleno incorrectamente el número de teléfono$/
     */
    public function rellenoIncorrectamenteElNumeroDeTelefono()
    {
        return array(
            new When("Relleno \"ninyo_telefono\" con \"96123\"")
        );
    }

    /**
     * @Then /^Debería ver un elemento de error para "([^"]*)"$/
     */
    public function deberiaVerUnElementoDeErrorPara($campo)
    {
        return new Then("I should see an \".error-{$campo}\" element");
    }

    /**
     * @Then /^Debería ver "([^"]*)"$/
     */
    public function deberiaVerTexto($texto)
    {
        return new Then("I should see \"$texto\"");
    }

    /**
     * @Given /^no haber guardado los datos$/
     */
    public function noHaberGuardadoLosDatos()
    {
        return array(
            new Given("Estoy en \"/ninyos\""),
            new Then("I should not see \"Ricardo\"")
        );
    }

    /**
     * @When /^relleno incorrectamente el dni$/
     */
    public function rellenoIncorrectamenteElDni()
    {
        return array(
            new When("Relleno \"ninyo_dni\" con \"32000000Z\"")
        );
    }

    /**
     * @When /^relleno incorrectamente el correo electrónico$/
     */
    public function rellenoIncorrectamenteElCorreoElectronico()
    {
        return array(
            new When("Relleno \"ninyo_email\" con \"ricardo@\""),
        );
    }

    /**
     * @When /^relleno incorrectamente la fecha de nacimiento$/
     */
    public function rellenoIncorrectamenteLaFechaDeNacimiento()
    {
        return array(
            new When("Relleno \"ninyo_fecha_nacimiento\" con \"07\""),
        );
    }

    /**
     * @Given /^Hay niños en el centro:$/
     */
    public function hayNinosEnElCentro(TableNode $table)
    {
        $em = $this->kernel->getContainer()->get('doctrine')->getEntityManager();
        $factory = $this->kernel->getContainer()->get('security.encoder_factory');

        foreach ($table->getHash() as $hash) {
            $user = new Ninyo();
            $encoder = $factory->getEncoder($user);
            $password = $encoder->encodePassword($hash['password'], $user->getSalt());
            $user->setPassword($password);
            $user->setNombre($hash['nombre']);
            $user->setEmail($hash['email']);
            $user->setApellidos($hash['apellidos']);
            $user->setFechaNacimiento(new \Datetime($hash['fecha_nacimiento']));
            $user->setDireccion($hash['direccion']);
            $user->setCodigoPostal($hash['cp']);
            $user->setPoblacion($hash['poblacion']);
            $user->setDni($hash['dni']);
            $user->setCentro($hash['centro']);
			$user->setTelefono($hash['telefono']);
			$user->setGrupo($em->getRepository('OpenGestiaBundle:Grupo')->findOneBy(
				array(
					'nombre' => $hash['grupo'],
					'centro' => $hash['centro']
				)
			));

            $em->persist($user);
        }

        $em->flush();
    }

    /**
     * @Given /^introduzca un dni ya existente en el mismo centro$/
     */
    public function introduzcaUnDniYaExistenteEnElMismoCentro()
    {
        return array(
            new When("Relleno \"ninyo_dni\" con \"12345678Z\"")
        );
    }

    /**
     * @When /^introduzca los datos de un niño ya existente en el mismo centro$/
     */
    public function introduzcaLosDatosDeUnNinoYaExistenteEnElMismoCentro()
    {
        return array(
            new When("Relleno \"ninyo_nombre\" con \"Luis\""),
            new When("Relleno \"ninyo_apellidos\" con \"Pérez Gil\""),
            new When("Relleno \"ninyo_fecha_nacimiento\" con \"19/02/2003\""),
            new When("Relleno \"ninyo_direccion\" con \"C/ Otra calle, 24\""),
            new When("Relleno \"ninyo_codigo_postal\" con \"46001\""),
            new When("Relleno \"ninyo_telefono\" con \"96654321\""),
            new When("Relleno \"ninyo_poblacion\" con \"Valencia\""),
            new When("selecciono \"Los lobos solitarios\" de \"ninyo_grupo\""),
        );
    }

    /**
     * @Then /^Debería ver un elemento de tipo "([^"]*)"$/
     */
    public function deberiaVerUnElementoDeTipo($tipo)
    {
        return new Then("I should see an \".{$tipo}\" element");
    }

    /**
     * @Given /^Debería estar en "([^"]*)"$/
     */
    public function deberiaEstarEn($url)
    {
        return new Then("I should be on \"$url\"");
    }

    /**
     * @When /^Estoy en la página de listado de niños de centro$/
     */
    public function estoyEnLaPaginaDeListadoDeNinosDeCentro()
    {
        return new Given("I am on \"/ninyo\"");
    }

    /**
     * @Then /^Debería ver los datos de los niños del centro$/
     */
    public function deberiaVerLosDatosDeLosNinosDelCentro()
    {
        $session = $this->getSession();

//	    ldd($session->getPage()->getContent());
        $webAssert = new WebAssert($session);

        $webAssert->elementsCount('css', 'table tr.ninyo', 3);
    }    

    /**
     * @Given /^No tengo niños en el centro$/
     */
    public function noTengoNinosEnElCentro()
    {
        $em = $this->kernel->getContainer()->get('doctrine')->getEntityManager();
        $users = $em->getRepository('OpenGestiaBundle:Ninyo')->findBy(array());

        foreach ($users as $user) {
            $em->remove($user);
        }

        $em->flush();
    }   

    /**
     * @Then /^No debería ver niños$/
     */
    public function noDeberiaVerNinos()
    {
        $session = $this->getSession();

        $webAssert = new WebAssert($session);

        $webAssert->elementsCount('css', 'table tr.ninyo', 0);
    }

    /**
     * @Given /^Estoy en la página de nuevos datos de contacto de educador$/
     */
    public function estoyEnLaPaginaDeNuevosDatosDeContactoDeEducador()
    {
        return new When("Estoy en \"/educador/new\"");
    }

    /**
     * @When /^rellene los datos del educador correctamente$/
     */
    public function relleneLosDatosDelEducadorCorrectamente()
    {
        return array(
            new When("Relleno \"educador_nombre\" con \"Ramón\""),
            new When("Relleno \"educador_apellidos\" con \"Sanchis Caballer\""),
            new When("Relleno \"educador_fecha_nacimiento\" con \"22/12/1987\""),
            new When("Relleno \"educador_direccion\" con \"C/ Azor, 4\""),
            new When("Relleno \"educador_codigo_postal\" con \"46010\""),
            new When("Relleno \"educador_telefono\" con \"96111222\""),
            new When("Relleno \"educador_poblacion\" con \"Valencia\""),
            new When("Relleno \"educador_email\" con \"rsanchis@opengestia.org\""),
	        new When("selecciono \"Las marmotas\" de \"educador_grupo\""),
            new When("Relleno \"educador_dni\" con \"32000000P\""),
            new When("Relleno \"educador_estudios\" con \"Ingeniería Informática\""),
            new When("Relleno \"educador_profesion\" con \"Desarrollador de software\""),
        );
    }

    /**
     * @Then /^Debería ver los datos del educador en el listado de educadores del centro$/
     */
    public function deberiaVerLosDatosDelEducadorEnElListadoDeEducadoresDelCentro()
    {
        $session = $this->getSession();

        $webAssert = new WebAssert($session);

        $webAssert->elementsCount('css', 'table tr.educador', 3);
    }

    /**
     * @When /^relleno incorrectamente el número de teléfono del educador$/
     */
    public function rellenoIncorrectamenteElNumeroDeTelefonoDelEducador()
    {
        return new When("Relleno \"educador_telefono\" con \"96123\"");
    }


    /**
     * @Given /^relleno incorrectamente el dni del educador$/
     */
    public function rellenoIncorrectamenteElDniDelEducador()
    {
        return new When("Relleno \"educador_dni\" con \"32000000Z\"");
    }

    /**
     * @Given /^no haber guardado los datos del educador$/
     */
    public function noHaberGuardadoLosDatosDelEducador()
    {
        return array(
            new Given("Estoy en \"/educador\""),
            new Then("I should not see \"Sanchis Caballer\"")
        );
    }

    /**
     * @Given /^relleno incorrectamente el correo electrónico del educador$/
     */
    public function rellenoIncorrectamenteElCorreoElectronicoDelEducador()
    {
        return new When("Relleno \"educador_email\" con \"mimail@\"");
    }

    /**
     * @Given /^relleno incorrectamente la fecha de nacimiento del educador$/
     */
    public function rellenoIncorrectamenteLaFechaDeNacimientoDelEducador()
    {
        return new When("Relleno \"educador_fecha_nacimiento\" con \"07/2222\"");
    }

	/**
	 * @When /^Estoy en la página de listado de educadores de centro$/
	 */
	public function estoyEnLaPaginaDeListadoDeEducadoresDeCentro()
	{
		return new When("Estoy en \"/educador\"");
	}

	/**
	 * @Then /^Debería ver los datos de los educadores del centro$/
	 */
	public function deberiaVerLosDatosDeLosEducadoresDelCentro()
	{
		$session = $this->getSession();

		$webAssert = new WebAssert($session);

		$webAssert->elementsCount('css', 'table tr.educador', 2);
	}

	/**
	 * @When /^Estoy en la página de ficha de un niño$/
	 */
	public function estoyEnLaPaginaDeFichaDeNino()
	{
		return array(
			new When("Estoy en la página de listado de niños de centro"),
			new When("sigo \"Ficha\"")
		);
	}
    /**
     * @Given /^Estoy en la página de ficha de un niño mediante ajax$/
     */
    public function estoyEnLaPaginaDeFichaDeUnNinoMedianteAjax()
    {
        $session = $this->getSession();

        $session->visit('http://opengestia.local/app_test.php/ninyo');

        $ficha_ninyo = $session->getPage()->find('css', 'a.ficha_ninyo');
        $ficha_ninyo->click();

        $session->wait(2000);
    }



	/**
	 * @When /^Lo marque como baja del curso$/
	 */
	public function loMarqueComoBajaDelCurso()
	{
		return new When('selecciono "Baja definitiva" de "ninyo_edit_estado"');
	}

	/**
	 * @Then /^No debería ver los datos del niño en el listado de niños del centro$/
	 */
	public function noDeberiaVerLosDatosDelNinoEnElListadoDeNinosDelCentro()
	{
		$session = $this->getSession();

		$webAssert = new WebAssert($session);

		$webAssert->elementsCount('css', 'table tr.ninyo', 2);
	}

	/**
	 * @Given /^Es un día del rango válido$/
	 */
	public function esUnDiaDelRangoValido()
	{
		$this->mocker->mockService('open_gestia.date_rank_checker')
			->shouldReceive('isRankValid')
			->andReturn(true);
		;
	}


	/**
	 * @Given /^Es un día del rango inválido$/
	 */
	public function esUnDiaDelRangoInvalido()
	{
		$this->mocker->mockService('open_gestia.date_rank_checker')
			->shouldReceive('isRankValid')
			->andReturn(false);
		;
	}

	/**
     * @Given /^Hay (\d+) niño de baja definitiva$/
     */
	public function hayNinoDeBajaDefinitiva($cantidad)
	{
		$this->mocker->mockService('open_gestia.date_rank_checker')
			->shouldReceive('isRankValid')
			->andReturn(true);
		;

		$em = $this->kernel->getContainer()->get('doctrine')->getEntityManager();
		$users = $em->getRepository('OpenGestiaBundle:Ninyo')->findAll();

		for($i = 0; $i < $cantidad; $i++)
		{
			$user = $users[$i];
			$user->setEstado(Persona::ESTADO_BAJA_DEFINITIVA);

			$em->persist($user);
		}
		$em->flush();
	}

	/**
	 * @Given /^Estoy en la página de listado de niños de baja definitiva de centro$/
	 */
	public function estoyEnLaPaginaDeListadoDeNinosDeBajaDefinitivaDeCentro()
	{
		return new When("Estoy en \"/ninyo/bajas-definitivas\"");
	}

	/**
	 * @When /^Seleccione la ficha del primer niño$/
	 */
	public function seleccioneLaFichaDelPrimerNino()
	{
		$session = $this->getSession();

		$session->getDriver()->click("//table//a");
	}

	/**
	 * @Given /^Marque el "([^"]*)" como alta del curso$/
	 */
	public function marqueElComoAltaDelCurso($tipo)
	{
		return new When('selecciono "Activo" de "'.$tipo.'_edit_estado"');
	}


	/**
	 * @Given /^Hay (\d+) educador de baja definitiva$/
	 */
	public function hayEducadorDeBajaDefinitiva($num)
	{
		$this->mocker->mockService('open_gestia.date_rank_checker')
			->shouldReceive('isRankValid')
			->andReturn(true);
		;

		$em = $this->kernel->getContainer()->get('doctrine')->getEntityManager();
		$users = $em->getRepository('OpenGestiaBundle:Educador')->findAll();

		for($i = 0; $i < $num; $i++)
		{
			$user = $users[$i];
			$user->setEstado(Persona::ESTADO_BAJA_DEFINITIVA);

			$em->persist($user);
		}

		$em->flush();
	}

	/**
	 * @Given /^Estoy en la página de listado de educadores de baja definitiva de centro$/
	 */
	public function estoyEnLaPaginaDeListadoDeEducadoresDeBajaDefinitivaDeCentro()
	{
		return new When("Estoy en \"/educador/bajas-definitivas\"");
	}

	/**
	 * @When /^Seleccione la ficha del primer educador$/
	 */
	public function seleccioneLaFichaDelPrimerEducador()
	{
		$session = $this->getSession();

		$session->getDriver()->click("//table//a");
	}

	/**
	 * @Then /^Debería ver los datos de los niños de baja definitiva del centro$/
	 */
	public function deberiaVerLosDatosDeLosNinosDeBajaDefinitivaDelCentro()
	{
		$session = $this->getSession();

		$webAssert = new WebAssert($session);

		$webAssert->addressEquals("/ninyo/bajas-definitivas");
		$webAssert->elementsCount('css', 'table tr.ninyo', 3);
	}

	/**
	 * @Given /^Hay (\d+) educadores de baja definitiva$/
	 */
	public function hayEducadoresDeBajaDefinitiva($num)
	{
		return new Then("Hay {$num} educador de baja definitiva");
	}

	/**
	 * @Given /^Hay (\d+) niños de baja definitiva$/
	 */
	public function hayNinosDeBajaDefinitiva($num)
	{
		return new Then("Hay {$num} niño de baja definitiva");
	}

	/**
	 * @Then /^Debería ver los datos de los educadores de baja definitiva del centro$/
	 */
	public function deberiaVerLosDatosDeLosEducadoresDeBajaDefinitivaDelCentro()
	{
		$session = $this->getSession();

		$webAssert = new WebAssert($session);

		$webAssert->addressEquals("/educador/bajas-definitivas");
		$webAssert->elementsCount('css', 'table tr.educador', 2);
	}

	/**
	 * @Given /^actualice los datos del educador correctamente$/
	 */
	public function actualiceLosDatosDelEducadorCorrectamente()
	{
		return array(
			new When("Relleno \"educador_edit_nombre\" con \"Ramón\""),
			new When("Relleno \"educador_edit_apellidos\" con \"Apellidos actualizados\""),
			new When("Relleno \"educador_edit_fecha_nacimiento\" con \"22/12/1987\""),
			new When("Relleno \"educador_edit_direccion\" con \"C/ Azor, 4\""),
			new When("Relleno \"educador_edit_codigo_postal\" con \"46010\""),
			new When("Relleno \"educador_edit_telefono\" con \"96111222\""),
			new When("Relleno \"educador_edit_poblacion\" con \"Valencia\""),
			new When("Relleno \"educador_edit_email\" con \"rsanchis@opengestia.org\""),
			new When("selecciono \"Las marmotas\" de \"educador_edit_grupo\""),
			new When("Relleno \"educador_edit_dni\" con \"32000000P\""),
			new When("Relleno \"educador_edit_estudios\" con \"Ingeniería Informática\""),
			new When("Relleno \"educador_edit_profesion\" con \"Desarrollador de software\""),
		);
	}

	/**
	 * @Then /^Debería ver los datos del educador actualizados en el listado de educadores del centro$/
	 */
	public function deberiaVerLosDatosDelEducadorActualizadosEnElListadoDeEducadoresDelCentro()
	{
		return array(
			new Then("Estoy en \"/educador/\""),
			new Then("I should see \"Ramón\""),
			new Then("I should see \"Apellidos actualizados\"")
		);
	}

	/**
	 * @Given /^relleno incorrectamente el número de teléfono al actualizar el educador$/
	 */
	public function rellenoIncorrectamenteElNumeroDeTelefonoAlActualizarElEducador()
	{
		return array(
			new When("Relleno \"educador_edit_telefono\" con \"9612\"")
		);
	}

	/**
	 * @Given /^relleno incorrectamente el dni al actualizar el educador$/
	 */
	public function rellenoIncorrectamenteElDniAlActualizarElEducador()
	{
		return array(
			new When("Relleno \"educador_edit_dni\" con \"32000000Z\"")
		);
	}

	/**
	 * @Given /^relleno incorrectamente el correo electrónico al actualizar el educador$/
	 */
	public function rellenoIncorrectamenteElCorreoElectronicoAlActualizarElEducador()
	{
		return array(
			new When("Relleno \"educador_edit_email\" con \"ricardo@\"")
		);
	}

	/**
	 * @Given /^relleno incorrectamente la fecha de nacimiento al actualizar el educador$/
	 */
	public function rellenoIncorrectamenteLaFechaDeNacimientoAlActualizarElEducador()
	{
		return array(
			new When("Relleno \"educador_edit_fecha_nacimiento\" con \"07\"")
		);
	}

	/**
	 * @Given /^Estoy en la página de ficha de educador$/
	 */
	public function estoyEnLaPaginaDeFichaDeEducador()
	{
		return array(
			new When("Estoy en la página de listado de educadores de centro"),
			new When("I follow \"Ficha\"")
		);
	}
	/**
	 * @When /^Marque el educador como baja del curso$/
	 */
	public function marqueElEducadorComoBajaDelCurso()
	{
		return new When('selecciono "Baja" de "educador_edit_estado"');
	}

	/**
	 * @Then /^No debería ver los datos del educador en el listado de educadores del centro$/
	 */
	public function noDeberiaVerLosDatosDelEducadorEnElListadoDeEducadoresDelCentro()
	{
		$session = $this->getSession();

		$webAssert = new WebAssert($session);

		$webAssert->addressEquals("/educador/");
		$webAssert->elementsCount('css', 'table tr.educador', 1);
	}

	/**
	 * @Given /^Marque el educador como baja definitiva del curso$/
	 */
	public function marqueElEducadorComoBajaDefinitivaDelCurso()
	{
		return new When('selecciono "Baja definitiva" de "educador_edit_estado"');
	}

	/**
	 * @Given /^Hay (\d+) niño de baja$/
	 */
	public function hayNinoDeBaja($cantidad)
	{
		$this->mocker->mockService('open_gestia.date_rank_checker')
			->shouldReceive('isRankValid')
			->andReturn(true);
		;

		$em = $this->kernel->getContainer()->get('doctrine')->getEntityManager();
		$users = $em->getRepository('OpenGestiaBundle:Ninyo')->findAll();

		for($i = 0; $i < $cantidad; $i++)
		{
			$user = $users[$i];
			$user->setEstado(Persona::ESTADO_BAJA);

			$em->persist($user);
		}
		$em->flush();
	}

	/**
	 * @Given /^Hay (\d+) educador de baja$/
	 */
	public function hayEducadorDeBaja($num)
	{
		$this->mocker->mockService('open_gestia.date_rank_checker')
			->shouldReceive('isRankValid')
			->andReturn(true);
		;

		$em = $this->kernel->getContainer()->get('doctrine')->getEntityManager();
		$users = $em->getRepository('OpenGestiaBundle:Educador')->findAll();

		for($i = 0; $i < $num; $i++)
		{
			$user = $users[$i];
			$user->setEstado(Persona::ESTADO_BAJA);

			$em->persist($user);
		}

		$em->flush();
	}

	/**
	 * @Given /^Estoy en la página de listado de niños de baja de centro$/
	 */
	public function estoyEnLaPaginaDeListadoDeNinosDeBajaDeCentro()
	{
		return new When("Estoy en \"/ninyo/bajas\"");
	}

	/**
	 * @Given /^Estoy en la página de listado de educadores de baja de centro$/
	 */
	public function estoyEnLaPaginaDeListadoDeEducadoresDeBajaDeCentro()
	{
		return new When("Estoy en \"/educador/bajas\"");
	}

	/**
	 * @Given /^el usuario tiene la configuración por defecto$/
	 */
	public function elUsuarioTieneLaConfiguracionPorDefecto()
	{
		$usuario =  $this->kernel->getContainer()->get('doctrine')->getEntityManager()->getRepository('OpenGestiaBundle:Persona')->findOneByEmail('pepe@opengestia.org');
		$usuario->setConfiguracion(new ConfiguracionUsuario());
	}

	/**
	 * @Given /^Veo los campos del listado de "([^"]*)" definidos por defecto$/
	 */
	public function veoLosCamposDelListadoDefinidosPorDefecto($tipoListado)
	{
		$configuracionDefecto = new ConfiguracionUsuario();

		$session = $this->getSession();
		foreach($configuracionDefecto->getCampos($tipoListado) as $campo)
		{
			$webAssert = new WebAssert($session);

			$webAssert->elementsCount('css', 'th.campo-'.$campo, 1);
		}
	}

	/**
     * @Given /^añado el campo "([^"]*)" para "([^"]*)"$/
     */
	public function anadoElCampo($campo, $tipo)
	{
		return new When("voy a \"/$tipo/anyadir-campo/$campo\"");
	}

	/**
	 * @Given /^Veo los campos del listado de "([^"]*)" definidos por defecto más los siguientes campos:$/
	 */
	public function veoLosCamposDelListadoDefinidosPorDefectoMasLosSiguientesCampos($tipoListado, PyStringNode $string)
	{
		//return new Then("imprime la última respuesta");

		$configuracionDefecto = new ConfiguracionUsuario();
		$session = $this->getSession();
		$campos = $configuracionDefecto->getCampos($tipoListado);

		foreach($string->getLines() as $linea)
		{
			$campos[] = $linea;
		}

		foreach($campos as $campo)
		{
			$webAssert = new WebAssert($session);

			$webAssert->elementsCount('css', 'th.campo-'.$campo, 1);
		}
	}

	/**
	 * @Given /^Veo los campos del listado de "([^"]*)" definidos por defecto excepto los siguientes campos:$/
	 */
	public function veoLosCamposDelListadoDeNinyosDefinidosPorDefectoExceptoLosSiguientesCampos($tipoListado, PyStringNode $string)
    {
		$configuracionDefecto = new ConfiguracionUsuario();
        $session = $this->getSession();
        $campos = $configuracionDefecto->getCampos($tipoListado);

        $lineas = $string->getLines();

        foreach($campos as $campo)
        {
            if(!in_array($campo, $lineas))
            {
                $webAssert = new WebAssert($session);

                $webAssert->elementsCount('css', 'th.campo-'.$campo, 1);
            }
        }
    }

    /**
	 * @Given /^añado el campo "([^"]*)" desde el panel$/
	 */
	public function anadoElCampoDesdeElPanel($campo)
	{
		$session = $this->getSession();
		$page = $session->getPage();

        $el = $page->find('css', "#listado-campos");
        $el->mouseOver();

		$el = $page->find('css', "#anyadir-campo-$campo");
		$el->click();
	}

	/**
	 * @Given /^elimino el campo "([^"]*)"$/
	 */
	public function eliminoElCampo($campo)
	{
		return new When("voy a \"/ninyo/eliminar-campo/$campo\"");
	}

	/**
	 * @Given /^elimino el campo "([^"]*)" desde el panel$/
	 */
	public function eliminoElCampoDesdeElPanel($campo)
	{
		$session = $this->getSession();
		$page = $session->getPage();

//		$el = $page->find('css', "#eliminar-campo-$campo");
//		$el->click();
	}

	/**
	 * @Given /^Un grupo definido en el centro$/
	 */
	public function unGrupoDefinidoEnElCentro()
	{
		$em = $this->kernel->getContainer()->get('doctrine')->getEntityManager();

		$grupo = new Grupo();
		$grupo->setTipo(Grupo::GRUPO_IDENTIDAD);
		$grupo->setNombre("Los lobos solitarios");
		$grupo->setCentro('micentro');

		$em->persist($grupo);
		$em->flush();
	}

	/**
	 * @Given /^Estoy en la página de nuevo grupo$/
	 */
	public function estoyEnLaPaginaDeNuevoGrupo()
	{
		return new When("estoy en \"/grupo/new\"");
	}

	/**
	 * @Given /^selecciono el tipo de grupo de los disponibles$/
	 */
	public function seleccionoElTipoDeGrupoDeLosDisponibles()
	{
		return new When('selecciono "Experiencia" de "grupo_tipo"');
	}

    /**
     * @Given /^Estoy en la página de nuevo grupo mediante ajax$/
     */
    public function estoyEnLaPaginaDeNuevoGrupoMedianteAjax()
    {
        $session = $this->getSession();

        $session->visit('http://opengestia.local/app_test.php/grupo');

        $boton_nuevo_grupo = $session->getPage()->find('css', 'a#btn_alta');
        $boton_nuevo_grupo->click();

        $session->wait(2000);
    }

    /**
	 * @Given /^asigno un nombre al nuevo grupo$/
	 */
	public function asignoUnNombreAlNuevoGrupo()
	{
		return new Given("relleno \"grupo_nombre\" con \"Los caballeros negros\"");
	}

	/**
	 * @Given /^Debería haber guardado el grupo correctamente$/
	 */
	public function deberiaHaberGuardadoElGrupoCorrectamente()
	{
		$em = $this->kernel->getContainer()->get('doctrine')->getEntityManager();
		$grupos = $em->getRepository('OpenGestiaBundle:Grupo')->findAll();

		if(count($grupos) !== 3)
			throw new \Exception("El grupo no ha sido guardado");
	}

	/**
	 * @Given /^Ver el listado de grupos del centro$/
	 */
	public function verElListadoDeGruposDelCentro()
	{
		return new When("estoy en \"/grupo\"");
	}

	/**
	 * @Given /^asigno un nombre ya usado al nuevo grupo$/
	 */
	public function asignoUnNombreYaUsadoAlNuevoGrupo()
	{
		return new Given("relleno \"grupo_nombre\" con \"Los lobos solitarios\"");
	}

	/**
	 * @Given /^No debería haber guardado el grupo correctamente$/
	 */
	public function noDeberiaHaberGuardadoElGrupoCorrectamente()
	{
		$em = $this->kernel->getContainer()->get('doctrine')->getEntityManager();
		$grupos = $em->getRepository('OpenGestiaBundle:Grupo')->findAll();

		if(count($grupos) !== 3)
			throw new \Exception("El grupo no debería haber sido guardado");
	}

	/**
	 * @Given /^Intento ascender un niño que no existe$/
	 */
	public function intentoAscenderUnNinoQueNoExiste()
	{
		return new When("estoy en \"/ninyo/ascender-a-educador/0\"");
	}

	/**
	 * @Given /^Hago click en ascender niño$/
	 */
	public function hagoClickEnAscenderNino()
	{
		return new When("sigo \"Ascender a educador\"");
	}

    /**
     * @Given /^Confirmo la acción de ascender$/
     */
    public function confirmoLaAccionDeAscender()
    {
        $session = $this->getSession();

        $ascender_button = $session->getPage()->find('css', '.ascender-ninyo-button');
        $ascender_button ->click();

        $session->wait(2000);
    }

    /**
     * @Given /^selecciono una persona del listado$/
     */
    public function seleccionoUnaPersonaDelListado()
    {
        $session = $this->getSession();

        $tableTrs = $session->getPage()->findAll('css', '.dataTable tbody tr');
        $personaTr = $tableTrs[count($tableTrs)-1];
        $personaCheckbox = $personaTr->find('css', 'input[type="checkbox"]');
        $personaCheckbox->check();
    }

    /**
     * @Given /^hago click en baja desde el panel$/
     */
    public function hagoClickEnBajaDesdeElPanel()
    {
        $session = $this->getSession();

        $desplegable_baja = $session->getPage()->find('css', '#btn_baja');
        $desplegable_baja->click();

        $boton_baja_temporal = $session->getPage()->find('css', '.baja_temporal_simultanea');
        $boton_baja_temporal->click();

        $session->wait(2000);
    }

    /**
     * @Given /^hago click en baja definitiva desde el panel$/
     */
    public function hagoClickEnBajaDefinitivaDesdeElPanel()
    {
        $session = $this->getSession();

        $desplegable_baja = $session->getPage()->find('css', '#btn_baja');
        $desplegable_baja->click();

        $boton_baja_definitiva = $session->getPage()->find('css', '.baja_definitiva_simultanea');
        $boton_baja_definitiva->click();

        $session->wait(2000);
    }

    /**
     * @Given /^Selecciono niños de baja definitiva desde el panel$/
     */
    public function seleccionoNinosDeBajaDesdeElPanel()
    {
        $session = $this->getSession();

        $menu = $session->getPage()->find('css', '#ninyos-menu');
        $link = $menu->find('css', '.baja-definitiva');

        $link->click();
    }

    /**
     * @Given /^Selecciono educadores de baja definitiva desde el panel$/
     */
    public function seleccionoEducadoresDeBajaDefinitivaDesdeElPanel()
    {
        $session = $this->getSession();

        $menu = $session->getPage()->find('css', '#educadores-menu');
        $link = $menu->find('css', '.baja-definitiva');

        $link->click();
    }

    /**
     * @Given /^No debería ver los datos del ninyo en el listado de ninyos del centro$/
     */
    public function noDeberiaVerLosDatosDelNinyoEnElListadoDeEducadoresDelCentro()
    {
        throw new PendingException();
    }


}