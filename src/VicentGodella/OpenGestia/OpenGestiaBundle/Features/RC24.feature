# language: es
Característica: RC24-Listado-Educadores-Centro
	Como responsable de centro
	Quiero ver un listado de educadores del centro
	Para consultar los datos de contacto de los educadores actuales

 	Antecedentes:
		Dados prepara antecedentes

	@orm
	Escenario: muestra los educadores que hay en el centro
		Dado Un usuario autenticado como "responsable de centro"
		 Cuando Estoy en la página de listado de educadores de centro
		 Entonces Debería ver los datos de los educadores del centro
