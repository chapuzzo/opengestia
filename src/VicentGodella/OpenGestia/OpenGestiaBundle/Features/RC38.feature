# language: es
Característica: RC38-Baja-Niños
	  Como responsable de centro
	Quiero dar de baja varios niños simultáneamente
	  Para ejecutar la baja de varios niños cómodamente

 	Antecedentes:
		Dados prepara antecedentes

    @orm @javascript
	Escenario: marca niños como baja correctamente
		Dados Un usuario autenticado como "responsable de centro"
	  	    Y Estoy en la página de listado de niños de centro
	   Cuando selecciono una persona del listado
		    Y hago click en baja desde el panel
	 Entonces No debería ver los datos del niño en el listado de niños del centro
