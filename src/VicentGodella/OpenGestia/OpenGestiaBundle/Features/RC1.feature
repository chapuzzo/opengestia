# language: es
Característica: RC1-Añadir-Grupo-Centro
	Como responsable de centro
	Quiero añadir el nombre de un grupo al centro
	Para tener un control de los grupos que tengo en el centro

	Antecedentes:
		Dados prepara antecedentes

	@orm @javascript
	Escenario: guarda un nuevo grupo correctamente
		 Dado Un usuario autenticado como "responsable de centro"
			Y Estoy en la página de nuevo grupo mediante ajax
	   Cuando selecciono el tipo de grupo de los disponibles
			Y asigno un nombre al nuevo grupo
			Y Haga click en guardar
	 Entonces Debería haber guardado el grupo correctamente
			Y Ver el listado de grupos del centro

	@orm @javascript
	Escenario: error al intentar guardar un grupo cuyo nombre ya existe
		 Dado Un usuario autenticado como "responsable de centro"
			Y Un grupo definido en el centro
	   Cuando Estoy en la página de nuevo grupo mediante ajax
			Y selecciono el tipo de grupo de los disponibles
			Y asigno un nombre ya usado al nuevo grupo
			Y Haga click en guardar
	 Entonces No debería haber guardado el grupo correctamente
			Y Debería ver un elemento de error para "nombre"