<?php

namespace VicentGodella\OpenGestia\OpenGestiaBundle\Service;

/**
 * @author Vicent Soria <vicent@opengestia.org>
 */
class DateRankChecker
{
	protected $rank = array('iniDate' => '01/09','endDate' => '31/03');
	protected $dater;

	public function __construct($dater)
	{
		$this->dater = $dater;
	}

	public function setRank($rank)
	{
		$this->rank = $rank;
	}

	public function setDater($dater)
	{
		$this->dater = $dater;
	}

	public function isRankValid()
	{
		list($iniDay, $iniMonth) = explode('/', $this->rank['iniDate']);
		list($endDay, $endMonth) = explode('/', $this->rank['endDate']);

		$normalizedIniDate = $iniMonth.'/'.$iniDay;
		$normalizedEndDate = $endMonth.'/'.$endDay;


		list($actualDay, $actualMonth) = explode('/', $this->dater->getCurrentTime('d/m'));

		$normalizedActualDate = $actualMonth.'/'.$actualDay;

		if($normalizedIniDate > $normalizedEndDate)
		{
			if($normalizedActualDate < $normalizedIniDate && $normalizedActualDate > $normalizedEndDate)
				return false;
			else
				return true;
		}
		return false;
	}
}
